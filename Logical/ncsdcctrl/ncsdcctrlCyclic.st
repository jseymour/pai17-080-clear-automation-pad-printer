
PROGRAM _CYCLIC
        
(* ### BEGIN DialAxis ### *)
        
(* life counter emulation of digital inputs *)
        DialAxis_DiDoIf.iLifeCntReference := DialAxis_DiDoIf.iLifeCntReference + 1;
        DialAxis_DiDoIf.iLifeCntNegHwEnd  := DialAxis_DiDoIf.iLifeCntNegHwEnd  + 1;
        DialAxis_DiDoIf.iLifeCntPosHwEnd  := DialAxis_DiDoIf.iLifeCntPosHwEnd  + 1;
        
(* ### END DialAxis ### *)

END_PROGRAM

