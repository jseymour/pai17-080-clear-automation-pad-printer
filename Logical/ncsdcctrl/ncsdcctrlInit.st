
PROGRAM _INIT
        
(* ### BEGIN DialAxis ### *)
        
(* initialize variables *)
        DialAxis_HW.DrvIf_Typ := ncSDC_DRVSERVO16;
        brsstrcpy(ADR(DialAxis_HW.DrvIf_Name[0]), ADR('DialAxis_DrvIf'));
        DialAxis_HW.DiDoIf_Typ := ncSDC_DIDO;
        brsstrcpy(ADR(DialAxis_HW.DiDoIf_Name[0]), ADR('DialAxis_DiDoIf'));
         
(* force variable offset generation *)
        DialAxis.size                      := DialAxis.size;
        DialAxis_DrvIf.iLifeCnt            := DialAxis_DrvIf.iLifeCnt;
        DialAxis_DiDoIf.iLifeCntDriveReady := DialAxis_DiDoIf.iLifeCntDriveReady;
        DialAxis_ModuleOk                  := DialAxis_ModuleOk;
         
(* assign your hardware inputs here*)
  
(*      DialAxis_DiDoIf.iPosHwEnd  := DialAxis_DiDoIf.iPosHwEnd;
        DialAxis_DiDoIf.iNegHwEnd  := DialAxis_DiDoIf.iNegHwEnd;
        DialAxis_DiDoIf.iReference := DialAxis_DiDoIf.iReference;
 *)
        
(* ### END DialAxis ### *)

END_PROGRAM

