
TYPE
	Steps : 
		(
		IO_NOT_FORCED,
		IO_WAIT_FORCE_VALUE,
		IO_WAIT_FORCE_ENABLE,
		IO_FORCING_ENABLED,
		IO_CHANGE_FORCE_VALUE,
		IO_DISABLE_FORCING
		);
	Channel_typ : 	STRUCT 
		Step : Steps;
		Datapoint : STRING[100];
		FB : Channel_FB_typ;
	END_STRUCT;
	Channel_FB_typ : 	STRUCT 
		EnableForcing : AsIOEnableForcing;
		DisableForcing : AsIODisableForcing;
		SetForceValue : AsIOSetForceValue;
		DatapointStatus : AsIODPStatus;
	END_STRUCT;
END_TYPE
