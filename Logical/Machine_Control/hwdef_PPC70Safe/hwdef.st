PROGRAM _INIT

	// require presence of IO modules when using real target
	IO_ModuleRequired[0] := 1; // X20BC
	IO_ModuleRequired[1] := 1; // X20PS
	IO_ModuleRequired[2] := 1; // X20DI
	IO_ModuleRequired[3] := 1; // X20DI
	IO_ModuleRequired[4] := 1; // X20DI
	IO_ModuleRequired[5] := 1; // X20DO
	IO_ModuleRequired[6] := 1; // X20DO
	IO_ModuleRequired[7] := 1; // X20DO
//	IO_ModuleRequired[8] := 1; // X20SI
//	IO_ModuleRequired[9] := 1; // X20SO
//	IO_ModuleRequired[10] := 1; // X20BT
//	IO_ModuleRequired[11] := 1; // X67DM
//	IO_ModuleRequired[12] := 0; 
	
	// usb browse flag
	USBBrowseRequired := 1;
	
	// set ethernet device for AsARCfg functions
	strcpy( ADR(EthernetIF), ADR('IF2') );

	// set visualization name
	strcpy( ADR(VisObjName), ADR('VisWVG') );
	
	// set usb drive file device parameter strng
	strcpy( ADR(FileDeviceParam), ADR('/DEVICE=IF3.ST1'));
	
	//Define hardware configuration
	HwCfg := PPC70_SAFETY;
	
	//Safety Settings
//	SLXUsed := TRUE; //If using an SLX type safety controller
//	SafetyStation := 'IF1.ST1.IF1.ST7'; // Module address of the safety controller
	
END_PROGRAM


PROGRAM _CYCLIC


(* TODO : Add your code here *)


END_PROGRAM
