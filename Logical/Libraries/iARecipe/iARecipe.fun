(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Library: IMRecipe
 * File: IMRecipe.fun
 * Author: sswindells
 * Created: October 11, 2010
 ********************************************************************
 * Functions and function blocks of library IMRecipe
 ********************************************************************)

FUNCTION_BLOCK iARecipeCyclic
	VAR_INPUT
		cmd : UINT;
		N : UINT;
		NDst : UINT;
		Name : STRING[30];
		Internal : REFERENCE TO iARecipe_Internal_typ;
		ActiveFileMismatch : REFERENCE TO BOOL;
		NActiveFile : REFERENCE TO UINT;
		MaxFiles : REFERENCE TO UINT;
		NewSys : REFERENCE TO BOOL;
	END_VAR
	VAR_OUTPUT
		Init : BOOL;
		Status : UINT; (*Status Information (Ready, Not Ready, Error)*)
		ErrorID : UINT; (*Primary Error Information*)
		ErrorID2 : UINT; (*Extended Error Information*)
		FileNames : ARRAY[0..999] OF STRING[30]; (*List of Raw Recipe Names*)
		FileNames_vc : ARRAY[0..999] OF STRING[35]; (*List of Recipe Names with '##.) ' in front*)
	END_VAR
	VAR CONSTANT
		MAXFILES : UINT := 1000; (*NOTE: IF YOU NEED MORE RECIPES MAKE SURE TO ADJUST THE ABOVE ARRAY SIZES TOO*)
	END_VAR
	VAR
		Step : UINT;
		StepTrace : ARRAY[0..99] OF UINT;
		i : UINT;
		numstring : STRING[5];
	END_VAR
	VAR CONSTANT
		STATE_IDLE : UINT := 0;
		STATE_INIT_SYS_INFO : UINT := 2;
		STATE_INIT_SYS_LOAD : UINT := 3;
		STATE_INIT_SYS_CREATE : UINT := 4;
		STATE_INIT_REC_INFO : UINT := 5;
		STATE_INIT_REC_INFO2 : UINT := 6;
		STATE_INIT_REC_READNAME : UINT := 7;
		STATE_INIT_REC_LOAD : UINT := 8;
		STATE_INIT_REC_LOAD2 : UINT := 9;
		STATE_FILE_LOAD : UINT := 10;
		STATE_FILE_CREATE : UINT := 15;
		STATE_FILE_SAVE : UINT := 20;
		STATE_FILE_COPY_LOAD : UINT := 30;
		STATE_FILE_COPY_CREATE : UINT := 31;
		STATE_FILE_COPY_SAVE : UINT := 32;
		STATE_FILE_RENAME_LOAD : UINT := 40;
		STATE_FILE_RENAME_CREATE : UINT := 41;
		STATE_FILE_RENAME_SAVE : UINT := 42;
		STATE_FILE_VIEW : UINT := 50;
		STATE_FILE_DELETE : UINT := 60;
		STATE_FILE_DELETE_ALL : UINT := 70;
		STATE_FILE_DELETE_ALL_WAIT : UINT := 71;
		STATE_FILE_IMPORT : UINT := 80;
		STATE_FILE_IMPORT_CREATE : UINT := 81;
		STATE_FILE_IMPORT_SAVE : UINT := 82;
		STATE_FILE_IMPORT_ALL : UINT := 90;
		STATE_FILE_IMPORT_ALL_CREATE : UINT := 91;
		STATE_FILE_IMPORT_ALL_SAVE : UINT := 92;
		STATE_FILE_EXPORT_LOAD : UINT := 100;
		STATE_FILE_EXPORT_SEARCH : UINT := 101;
		STATE_FILE_EXPORT_WRITE : UINT := 102;
		STATE_FILE_EXPORT_ALL : UINT := 110;
		STATE_FILE_EXPORT_ALL_LOAD : UINT := 111;
		STATE_FILE_EXPORT_ALL_SEARCH : UINT := 112;
		STATE_FILE_EXPORT_ALL_WRITE : UINT := 113;
		STATE_FILE_BCKGND_LOAD : UINT := 120;
		STATE_FILE_BCKGND_CREATE : UINT := 130;
		STATE_FILE_BCKGND_SAVE : UINT := 131;
		STATE_SYS_SAVE : UINT := 160;
		STATE_SYS_IMPORT : UINT := 170;
		STATE_SYS_IMPORT_SAVE : UINT := 171;
		STATE_SYS_EXPORT : UINT := 180;
		STATE_SYS_EXPORT2 : UINT := 181;
		STATE_DONE : UINT := 400;
		STATE_INTERNAL_ERROR : UINT := 500;
	END_VAR
	VAR
		TON_AutoSaveInterval : TON;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK iARecipeInit
	VAR_INPUT
		Config : REFERENCE TO iARecipe_Config_typ;
		Internal : REFERENCE TO iARecipe_Internal_typ;
	END_VAR
	VAR_OUTPUT
		Status : UINT; (*Status Information (Ready, Not Ready, Error)*)
	END_VAR
	VAR
		MemPartCreate : AsMemPartCreate;
		MemPartAlloc : AsMemPartAlloc;
	END_VAR
END_FUNCTION_BLOCK
