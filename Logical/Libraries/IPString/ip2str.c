#include "IPString.h"
#include <AsString.h>
#include <string.h>


void IP2Str(IP2Str_typ* inst){
	char buffer[16];
	if (inst->pIPString != 0) {

		/* convert first octet to string, place it in IPString and put a dot on the end of it */
		itoa((unsigned char)inst->Octet1, (unsigned long)buffer);
		strcpy((char*)inst->pIPString, buffer);
		strcat((char*)inst->pIPString, ".");

		itoa((unsigned char)inst->Octet2, (unsigned long)buffer);
		strcat((char*)inst->pIPString, buffer);
		strcat((char*)inst->pIPString, ".");

		itoa((unsigned char)inst->Octet3, (unsigned long)buffer);
		strcat((char*)inst->pIPString, buffer);
		strcat((char*)inst->pIPString, ".");

		itoa((unsigned char)inst->Octet4, (unsigned long)buffer);
		strcat((char*)inst->pIPString, buffer);

	}

}



