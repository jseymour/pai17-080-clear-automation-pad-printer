(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: defaults
 * File: defaults.st
 * Author: amusser
 * Created: January 06, 2012
 * Version: 1.00.0
 * Modified Date: 5/26/2016
 * Modified BY: mhanson
 ********************************************************************
 * Implementation OF PROGRAM DynIOmap

 * Description:

	This PROGRAM lists default settings, and it is possible TO DO so
	FOR several different configurations.

 * Options:

 * Version History:
 	1.0 -- first release
	1.1 --  Added safety settings

 ********************************************************************)

PROGRAM _CYCLIC
	IF (DataMgrInit) THEN
		IF (HMI.OEMRestoreDefaultsPB) THEN
			HMI.OEMRestoreDefaultsPB := 0;
					
			 // Dont allow the restore defaults button to clear this out. This will screw up the sync with the safe PLC
//			safePWInit := SystemSettings.SafetyPWInit;
//			
//			memset(ADR(SystemSettings), 0, SIZEOF(SystemSettings));
					
			SystemSettings.MaxRecipes := 25;
			SystemSettings.Language := 0;
			SystemSettings.UnitType := 0;
			SystemSettings.Brightness := 100;
			SystemSettings.LineVoltage := 115;
			SystemSettings.PhaseMonIgnore := 1;
			
			// SMTP Defaults
			SystemSettings.SMTP.ModelName 					:= ('Test String'); // Enter OEM Model Name
			SystemSettings.SMTP.IPParts[0]					:= 0;
			SystemSettings.SMTP.IPParts[1]					:= 0;
			SystemSettings.SMTP.IPParts[2]					:= 0;
			SystemSettings.SMTP.IPParts[3]					:= 0;
			SystemSettings.SMTP.Username					:= ('');
			SystemSettings.SMTP.Password					:= ('');
			SystemSettings.SMTP.SenderAdr					:= ('');
			SystemSettings.SMTP.SetInterval				:= T#8h;
			SystemSettings.SMTP.MessageData[1].FollowUp	:= T#8h;
			SystemSettings.SMTP.MessageData[1].SendInterval:= T#8h;
					
			// virtual axis 
			SystemSettings.AxisConfig[AXMASTER].Motor.Type 						:= MOTOR_NOTREQUIRED;
			SystemSettings.AxisConfig[AXMASTER].JogSpeed						:= 0.5; //deg/sec
			SystemSettings.AxisConfig[AXMASTER].JogSpeedHigh					:= 1.0; // deg/sec
			SystemSettings.AxisConfig[AXMASTER].JogIncrement					:= 0.1; // degs
			SystemSettings.AxisConfig[AXMASTER].Accel							:= 2; //deg/sec^2
			SystemSettings.AxisConfig[AXMASTER].Decel							:= 10; //deg/sec^2
			SystemSettings.AxisConfig[AXMASTER].Limits.AXLIM_T_JOLT				:= 0; 

			//	AlarmMgr.RestartRequired := 1;
			HMI.OEMSysSettingsStatus := 7;   // factory defaults restored
						
		END_IF
	END_IF
	
END_PROGRAM
