
TYPE
	Machine_typ : 	STRUCT 
		IO : IO_typ;
		State : Machine_States_enum;
		Prev_State : Machine_States_enum;
	END_STRUCT;
	DI_typ : 	STRUCT 
		E_Stop2 : BOOL;
		E_Stop3 : BOOL;
		E_Stop_Open : BOOL;
		Door_Open1 : BOOL;
		Door_Open2 : BOOL;
		Door_Open3 : BOOL;
		Door_Open4 : BOOL;
		Door_Relay_Open : BOOL;
		Light_Curtain_Open : BOOL;
		Air_Pressure_OK : BOOL;
		Reject_Vacuum_OK : BOOL;
		Holddown_Vacuum_OK : BOOL;
		Dial_In_Dwell : BOOL;
		Dial_At_Home : BOOL;
		Part_Rejected : BOOL;
		Reject_Bin_In_Place : BOOL;
		Reject_Bin_Full : BOOL;
		Part_Holddown_Extended : BOOL;
		Part_Holddown_Retracted : BOOL;
		Reject_Vertical_Extended : BOOL;
		Reject_Vertical_Retracted : BOOL;
		Reject_Horizontal_Extended : BOOL;
		Reject_Horizontal_Retracted : BOOL;
		TP_At_Home : BOOL;
		TP_Ready_To_Print : BOOL;
		TP_Running : BOOL;
		Cognex_Output3 : BOOL;
		Cognex_Output2 : BOOL;
		Cognex_Output1 : BOOL;
		Part_Present : BOOL;
	END_STRUCT;
	IO_typ : 	STRUCT 
		DigI : DI_typ;
		DigO : DO_typ;
		Module_OK : ARRAY[0..7]OF BOOL;
	END_STRUCT;
	DO_typ : 	STRUCT 
		Safety_Relay_Reset : BOOL;
		Open_Stack_Light : BOOL;
		Amber_Stack_Light : BOOL;
		Red_Stack_Light : BOOL;
		Air_Pressure_Enable : BOOL;
		Cognex_Input3 : BOOL;
		Cognex_Input2 : BOOL;
		Cognex_Input1 : BOOL;
		Lock_Doors : BOOL;
		Extend_Part_Holddown : BOOL;
		Retract_Part_Holddown : BOOL;
		Extend_Reject_Vertical : BOOL;
		Retract_Reject_Vertical : BOOL;
		Extend_Reject_Horizontal : BOOL;
		Retract_Reject_Horizontal : BOOL;
		Reject_Vacuum : BOOL;
		Holddown_Vacuum : BOOL;
		TP_Start_Cycle : BOOL;
		TP_Stop_Cycle : BOOL;
		TP_Run_End_Conveyor : BOOL;
		TP_Start_Pad_Cleaning : BOOL;
		TP_Start_Pad1 : BOOL;
		TP_Start_Pad2 : BOOL;
	END_STRUCT;
	Program_Interface_typ : 	STRUCT 
		Start : BOOL; (*Signal from main program to indicate machine is ready for station to begin operation*)
		Stop : BOOL; (*Signal from the main program to order the stations to stop their operations immediately*)
		Done : BOOL; (*Signal from the station to indicate station is done with operation and ready for next part*)
		Manual : BOOL; (*Flag from main program indicating machine is in manual operation mode*)
		Alarm : BOOL; (*Signal from the station to indicate and active alarm requiring operator attention*)
		AlarmMessage : STRING[80]; (*Alarm string to be shown on the HMI when this program encounters an error*)
		AlarmReset : BOOL; (*Signal from the main program signaling stations to clear currently active alarms*)
	END_STRUCT;
	Load_Unload_Station_typ : 	STRUCT 
		Interface : Program_Interface_typ;
	END_STRUCT;
	Inspection_Station_typ : 	STRUCT 
		Interface : Program_Interface_typ;
	END_STRUCT;
	Part_Verification_Station_typ : 	STRUCT 
		Interface : Program_Interface_typ;
	END_STRUCT;
	Reject_Station_typ : 	STRUCT 
		Interface : Program_Interface_typ;
	END_STRUCT;
	Print_Station_typ : 	STRUCT 
		Interface : Program_Interface_typ;
	END_STRUCT;
	Part_typ : 	STRUCT 
		Recipe : Recipe_typ;
		Present : BOOL;
		Reject : BOOL;
	END_STRUCT;
	Recipe_typ : 	STRUCT 
		Product_Name : STRING[80];
		TP_Number : USINT;
		Camera_Inspection_Number : USINT;
	END_STRUCT;
	Commands_typ : 	STRUCT 
		Cycle_Start : BOOL;
		Reset : BOOL;
		Cycle_Stop : BOOL;
		Manual : BOOL;
	END_STRUCT;
	Machine_States_enum : 
		(
		INIT,
		WAIT,
		INDEX,
		ERROR,
		MANUAL,
		STOP,
		ALARM
		);
	Dial_typ : 	STRUCT 
		Interface : Program_Interface_typ;
	END_STRUCT;
END_TYPE
