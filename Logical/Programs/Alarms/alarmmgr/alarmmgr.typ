
TYPE
	Steps : 
		(
		STEP_IDLE,
		STEP_FAULT_RESET
		);
	AlarmBits_Typ : 	STRUCT 
		Hardware : ARRAY[0..MAX_ALARM_HARDWARE_BIT]OF BOOL;
		Application : ARRAY[0..MAX_ALARM_APP_BIT]OF BOOL;
		Communication : ARRAY[0..MAX_ALARM_COMM_BIT]OF BOOL;
		Axis : ARRAY[0..MAX_ALARM_AXIS_BIT]OF BOOL;
		Safety : ARRAY[0..MAX_ALARM_SAFETY_BIT]OF BOOL;
	END_STRUCT;
	FaultLevel_Typ : 	STRUCT 
		Hardware : ARRAY[0..MAX_ALARM_HARDWARE_BIT]OF UINT;
		Application : ARRAY[0..MAX_ALARM_APP_BIT]OF UINT;
		Communication : ARRAY[0..MAX_ALARM_COMM_BIT]OF UINT;
		Axis : ARRAY[0..MAX_ALARM_AXIS_BIT]OF UINT;
		Safety : ARRAY[0..MAX_ALARM_SAFETY_BIT]OF UINT;
	END_STRUCT;
	ParameterFaultText_Typ : 	STRUCT 
		BasicTextID : STRING[50];
		GetTuningText : ArTextSysGetText;
		GetBasicText : ArTextSysGetText;
		GetLimitText : ArTextSysGetText;
		TuningTextID : STRING[50];
		LimitTextID : STRING[50];
	END_STRUCT;
END_TYPE
