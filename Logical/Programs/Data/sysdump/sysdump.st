(********************************************************************
 * COPYRIGHT -- iAutomation
 ********************************************************************
 * Program: sysdump
 * File: sysdump.st
 * Author: pholleger
 * Created: June 09, 2015
 * Version: 1.00.0
 * Modified Date: 5/25/2016
 * Modified BY: mhanson
 ********************************************************************
 * Implementation OF PROGRAM sysdump

 * Description:

 * Options:

 * Version History:
 	1.0 -- First release
	1.1 --  Added [sysdumpState := SD_WAIT;] to the error state. Will get stuck here without this if there is an error

 ********************************************************************)

PROGRAM _INIT

	DTStructureGetTime_0.enable := TRUE;
	DTStructureGetTime_0.pDTStructure := ADR(DateTime.num);

END_PROGRAM


PROGRAM _CYCLIC

	// Get current date and time for use in file name
	DTStructureGetTime_0();
	IF DTStructureGetTime_0.status = 0 THEN
		brsmemset(ADR(DateTime.str.full),0,SIZEOF(DateTime.str.full));
		brsitoa(UINT_TO_DINT(DateTime.num.year),ADR(DateTime.str.year));
		brsitoa(USINT_TO_DINT(DateTime.num.month),ADR(DateTime.str.month));
		brsitoa(USINT_TO_DINT(DateTime.num.day),ADR(DateTime.str.day));
		brsitoa(USINT_TO_DINT(DateTime.num.hour),ADR(DateTime.str.hour));
		brsitoa(USINT_TO_DINT(DateTime.num.minute),ADR(DateTime.str.minute));
		brsitoa(USINT_TO_DINT(DateTime.num.second),ADR(DateTime.str.second));
		//Prepare Date/Time as a String
		brsstrcat(ADR(DateTime.str.full),ADR(DateTime.str.year));		//YYYY
		brsstrcat(ADR(DateTime.str.full),ADR('-'));		//dash			//YYYY-
		brsstrcat(ADR(DateTime.str.full),ADR(DateTime.str.month));		//YYYY-MM
		brsstrcat(ADR(DateTime.str.full),ADR('-'));		//dash			//YYYY-MM-
		brsstrcat(ADR(DateTime.str.full),ADR(DateTime.str.day));		//YYYY-MM-DD
		brsstrcat(ADR(DateTime.str.full),ADR('_'));		//underscore	//YYYY-MM-DD_
		brsstrcat(ADR(DateTime.str.full),ADR(DateTime.str.hour));		//YYYY-MM-DD_hh
		brsstrcat(ADR(DateTime.str.full),ADR('-'));		//dash			//YYYY-MM-DD_hh-
		brsstrcat(ADR(DateTime.str.full),ADR(DateTime.str.minute));		//YYYY-MM-DD_hh-mm
		brsstrcat(ADR(DateTime.str.full),ADR('-'));		//dash			//YYYY-MM-DD_hh-mm-
		brsstrcat(ADR(DateTime.str.full),ADR(DateTime.str.second));		//YYYY-MM-DD_hh-mm-ss
	//If we can't get date/time because of an error, go here.
	ELSIF DTStructureGetTime_0.status < 65534 THEN
		DTStructureGetTime_0.enable := FALSE;
		//wait
	END_IF

	CASE sysdumpState OF

		SD_WAIT:

			IF SysDump.cmd THEN
				SysDump.cmd := FALSE;
				SysDump.inProgress := TRUE;
				sysdumpState := SD_DUMP;
			END_IF

		SD_DUMP:

			brsmemset(ADR(sd_fiName),0,SIZEOF(sd_fiName));
			brsstrcat(ADR(sd_fiName),ADR(G_BUILDNAME));			//project_config
			brsstrcat(ADR(sd_fiName),ADR('_'));	//underscore	//project_config_
			brsstrcat(ADR(sd_fiName),ADR(DateTime.str.full));	//project_config_YYYY-MM-DD_hh-mm-ss
			brsstrcat(ADR(sd_fiName),ADR('.tar.gz'));			//project_config_YYYY-MM-DD_hh-mm-ss.tar.gz
				//Above is the final standard configuration for the file name.
				//Examples with our current naming scheme (as of 6/10/2015) include:
				//UST_0101a_MSC_2015-6-10_1-59-54.tar.gz
				//td_0403a_APC910_X67_Brake_2015-6-10_1-59-54.tar.gz
			IF (brsstrlen(ADR(sd_fiName)) <= 250) THEN
				SdmSystemDump_0.enable := TRUE;
			ELSE	
				brsmemset(ADR(sd_fiName),0,SIZEOF(sd_fiName));
				brsmemset(ADR(truncBuildName),0,SIZEOF(truncBuildName));	//limited to 100 characters
				brsmemset(ADR(sd_fiName),0,SIZEOF(sd_fiName));
				brsstrcat(ADR(sd_fiName),ADR(truncBuildName));		//project_config
				brsstrcat(ADR(sd_fiName),ADR('_'));	//underscore	//project_config_
				brsstrcat(ADR(sd_fiName),ADR(DateTime.str.full));	//project_config_YYYY-MM-DD_hh-mm-ss
				brsstrcat(ADR(sd_fiName),ADR('.tar.gz'));			//project_config_YYYY-MM-DD_hh-mm-ss.tar.gz		
				SdmSystemDump_0.enable := TRUE;	
			END_IF
			SdmSystemDump_0.configuration := sdm_SYSTEMDUMP_DATA;
			SdmSystemDump_0.pDevice := ADR('usbdrive');
			SdmSystemDump_0.pFile := ADR(sd_fiName);	//empty means that this file will be saved in the form of "BuR_SDM_Sysdump_YYYY-MM-DD_hh-mm-ss.tar.gz"
			SdmSystemDump_0.pParam := ADR('');			//fill this parameter in for only a subset of the SDM

			IF SdmSystemDump_0.status = 0 THEN
				SdmSystemDump_0.enable := FALSE;
				sysdumpState := SD_END;
			ELSIF SdmSystemDump_0.status < 65534 THEN
				SdmSystemDump_0.enable := FALSE;
				sysdumpState := SD_ERROR;
				sysdumpErrNum := SdmSystemDump_0.status;
			END_IF

		SD_END:

			SysDump.inProgress := FALSE;
			SysDump.complete := TRUE;
			sysdumpState := SD_WAIT;

		SD_ERROR:
			//message handled by hmimgr
			SysDump.inError := TRUE;
			SysDump.inProgress := FALSE;
			SysDump.complete := FALSE;
			SysDump.cmd := FALSE;
			sysdumpState := SD_WAIT;

	END_CASE

	SdmSystemDump_0();

END_PROGRAM
