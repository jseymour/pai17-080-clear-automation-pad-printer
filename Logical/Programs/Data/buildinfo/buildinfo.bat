@ECHO OFF
ECHO !!! GENERATING DYNAMIC BUILDINFO PROGRAM FILE !!!

CD %~dp0
ECHO     File path is %CD%
SET prgfile=".\buildinfo.st"
SET varfile=".\buildinfo.var"
ECHO     Program file is %prgfile%
ECHO     Variablefile is %varfile%


FOR /f "tokens=2-4 delims=/ " %%a IN ('date /t') DO (set mydate=%%c.%%a.%%b)
FOR /f "tokens=1-2 delims=/:" %%a IN ('time /t') DO (set mytime=%%a:%%b)



ECHO (**********************************************************************) > %varfile%
ECHO (*   Dynamic Build Info header file generated: %mydate% %mytime%    *) >> %varfile%
ECHO (**********************************************************************) >> %varfile%
ECHO (**********************************************************************) >> %varfile%
ECHO (*    This file is generated with each build by the batch file*) >> %varfile%
ECHO (**********************************************************************) >> %varfile%
ECHO. >> %prgfile%
ECHO VAR >> %varfile%
ECHO BUILDINFO : STRING[200] := '%2::%3 by %1 %mydate% %mytime%'; >> %varfile%
ECHO BUILDNAME : STRING[200] := '%2_%3'; >> %varfile%
ECHO END_VAR >> %varfile%


ECHO (**********************************************************************) > %prgfile%
ECHO (*   Dynamic Build Info header file generated: %mydate% %mytime%    *) >> %prgfile%
ECHO (**********************************************************************) >> %prgfile%
ECHO (**********************************************************************) >> %prgfile%
ECHO (*    This file is generated with each build by the batch file*) >> %prgfile%
ECHO (**********************************************************************) >> %prgfile%

ECHO. >> %prgfile%
ECHO PROGRAM _INIT >> %prgfile%
ECHO BUILDINFO:=BUILDINFO; >> %prgfile%
ECHO BUILDNAME:=BUILDNAME; >> %prgfile%
ECHO END_PROGRAM >> %prgfile%
ECHO PROGRAM _CYCLIC >> %prgfile%
ECHO brsmemset(ADR(G_BUILDNAME),0,SIZEOF(G_BUILDNAME)); >> %prgfile%
ECHO brsstrcpy(ADR(G_BUILDNAME),ADR(BUILDNAME)); >> %prgfile%
ECHO brsmemset(ADR(G_BUILDINFO),0,SIZEOF(G_BUILDINFO)); >> %prgfile%
ECHO brsstrcpy(ADR(G_BUILDINFO),ADR(BUILDINFO)); >> %prgfile%
ECHO END_PROGRAM >> %prgfile%