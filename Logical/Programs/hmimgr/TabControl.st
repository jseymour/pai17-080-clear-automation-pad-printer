(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Program: hmimgr
 * File: TabControl.st
 * Author: SSwindells
 * Created: April 11, 2012
 ********************************************************************
 * Implementation of program hmimgr
 ********************************************************************) 

ACTION TabControl: 
			// MAIN TAB LAYER
		// set all tab backgrounds to the secondary (not visible) color
		// and hide the seperating line
	FOR i := 0 TO ( (SIZEOF(HMI.MainTabColorCDP)/SIZEOF(HMI.MainTabColorCDP[0]))-1) DO
		HMI.MainTabColorCDP[i] := HMIColors.InactiveTabColor + 256*0;
		SHOW(ADR(HMI.MainTabLineSDP[i]));
	END_FOR
		
	CASE HMI.Page.Current OF
		PAGE_MAIN:
			HMI.MainTabColorCDP[0] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.MainTabLineSDP[0]));
				
		PAGE_RECIPE:
			HMI.MainTabColorCDP[1] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.MainTabLineSDP[1]));
				
		PAGE_SERVICE1..PAGE_SERVICE9,PAGE_EMAIL1:
			HMI.MainTabColorCDP[2] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.MainTabLineSDP[2]));
				
		PAGE_SYS1..PAGE_SYS8,PAGE_EMAIL2:
			HMI.MainTabColorCDP[3] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.MainTabLineSDP[3]));
	END_CASE
				
	// service TAB LAYER
	// set all tab backgrounds to the secondary (not visible) color
	// and hide the seperating line
	FOR i := 0 TO ( (SIZEOF(HMI.ServiceTabColorCDP)/SIZEOF(HMI.ServiceTabColorCDP[0]))-1) DO
		HMI.ServiceTabColorCDP[i] := HMIColors.InactiveTabColor + 256*0;
		SHOW(ADR(HMI.ServiceTabLineSDP[i]));
	END_FOR
		
	CASE HMI.Page.Current OF
		PAGE_SERVICE1,PAGE_EMAIL1:
			HMI.ServiceTabColorCDP[0] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[0]));
				
		PAGE_SERVICE2:
			HMI.ServiceTabColorCDP[1] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[1]));
				
		PAGE_SERVICE3:
			HMI.ServiceTabColorCDP[2] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[2]));
				
		PAGE_SERVICE4:
			HMI.ServiceTabColorCDP[3] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[3]));
			
		PAGE_SERVICE5:
			HMI.ServiceTabColorCDP[4] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[4]));
		
		PAGE_SERVICE6:
			HMI.ServiceTabColorCDP[5] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[5]));
			
		PAGE_SERVICE7:
			HMI.ServiceTabColorCDP[6] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[6]));
			
		PAGE_SERVICE8:
			HMI.ServiceTabColorCDP[7] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[7]));
			
		PAGE_SERVICE9:
			HMI.ServiceTabColorCDP[7] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.ServiceTabLineSDP[7]));
		
	END_CASE
	
	// system TAB LAYER
	// set all tab backgrounds to the secondary (not visible) color
	// and hide the seperating line
	FOR i := 0 TO ( (SIZEOF(HMI.SysTabColorCDP)/SIZEOF(HMI.SysTabColorCDP[0]))-1) DO
		HMI.SysTabColorCDP[i] := HMIColors.InactiveTabColor + 256*0;
		SHOW(ADR(HMI.SysTabLineSDP[i]));
	END_FOR
		
	CASE HMI.Page.Current OF
		PAGE_SYS1:
			HMI.SysTabColorCDP[0] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[0]));
				
		PAGE_SYS2:
			HMI.SysTabColorCDP[1] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[1]));
				
		PAGE_SYS3:
			HMI.SysTabColorCDP[2] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[2]));
				
		PAGE_SYS4:
			HMI.SysTabColorCDP[3] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[3]));
			
		PAGE_SYS5:
			HMI.SysTabColorCDP[4] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[4]));
				
		PAGE_SYS6:
			HMI.SysTabColorCDP[5] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[5]));
				
		PAGE_SYS7:
			HMI.SysTabColorCDP[6] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[6]));
				
		PAGE_SYS8:
			HMI.SysTabColorCDP[7] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[7]));
		
		PAGE_EMAIL2: 
			HMI.SysTabColorCDP[1] := HMIColors.ActiveTabColor + 256*0;
			HIDE(ADR(HMI.SysTabLineSDP[1]));
		
	END_CASE			
END_ACTION