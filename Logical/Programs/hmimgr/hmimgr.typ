
TYPE
	Steps : 
		(
		STEP_IDLE,
		STEP_NEW_RECIPE,
		STEP_NEW_RECIPE_WAIT,
		STEP_LOAD_RECIPE,
		STEP_LOAD_RECIPE_WAIT,
		STEP_SAVE_RECIPE,
		STEP_SAVE_RECIPE_WAIT,
		STEP_COPY_RECIPE,
		STEP_COPY_RECIPE_WAIT,
		STEP_RENAME_RECIPE,
		STEP_RENAME_RECIPE_WAIT,
		STEP_DELETE_RECIPE,
		STEP_DELETE_RECIPE_WAIT,
		STEP_DELETE_RECIPES,
		STEP_DELETE_RECIPES_WAIT,
		STEP_IMPORT_RECIPE,
		STEP_IMPORT_RECIPE_WAIT,
		STEP_IMPORT_RECIPES,
		STEP_IMPORT_RECIPES_WAIT,
		STEP_EXPORT_RECIPE,
		STEP_EXPORT_RECIPE_WAIT,
		STEP_EXPORT_RECIPES,
		STEP_EXPORT_RECIPES_WAIT,
		STEP_USB_BROWSE0,
		STEP_USB_BROWSE1,
		STEP_USB_BROWSE2,
		STEP_USB_BROWSE3,
		STEP_USB_BROWSE4,
		STEP_USB_BROWSE5,
		STEP_IMPORT_SYS,
		STEP_IMPORT_SYS_WAIT,
		STEP_EXPORT_SDM,
		STEP_EXPORT_SDM_WAIT,
		STEP_EXPORT_SYS,
		STEP_EXPORT_SYS_WAIT,
		STEP_ETHERNET_SETTINGS_LOAD_MODE,
		STEP_ETHERNET_SETTINGS_LOAD_IP,
		STEP_ETHERNET_SETTINGS_LOAD_SNM,
		STEP_ETHERNET_SETTINGS_LOAD_GW,
		STEP_ETHERNET_SETTINGS_LOAD_INA,
		STEP_ETHERNET_SETTINGS_SAVE_MODE,
		STEP_ETHERNET_SETTINGS_SAVE_IP,
		STEP_ETHERNET_SETTINGS_SAVE_SNM,
		STEP_ETHERNET_SETTINGS_SAVE_GW,
		STEP_ETHERNET_SETTINGS_SAVE_INA,
		STEP_EMAIL_CONFIG0,
		STEP_EMAIL_CONFIG1
		);
	HMIColors_typ : 	STRUCT 
		PageBackColor : USINT;
		ActiveTabColor : USINT;
		InactiveTabColor : USINT;
	END_STRUCT;
END_TYPE
