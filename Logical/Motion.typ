
TYPE
	UnitBasis_enum : 
		(
		UNITBASIS_FINITE,
		UNITBASIS_ENDLESS_PERIODIC,
		UNITBASIS_ENDLESS_NONPERIODIC
		);
	MC_Data_AxisLimits_typ : 	STRUCT 
		AXLIM_V : REAL;
		AXLIM_A : REAL;
		AXLIM_POS_SW_END : DINT;
		AXLIM_NEG_SW_END : DINT;
		AXLIM_DS_STOP : REAL;
	END_STRUCT;
	MC_Data_ACMotorPar_typ : 	STRUCT 
		Voltage : REAL;
		RatedSpeed : REAL;
		MaxSpeed : REAL;
		Rs : REAL;
		Rr : REAL;
		Lm : REAL;
		Ls : REAL;
		Lr : REAL;
		zp : INT;
		I_stall : REAL;
		I_rated : REAL;
		I_mag : REAL;
		I_max : REAL;
		T_stall : REAL;
		T_rated : REAL;
		T_max : REAL;
		Kt : REAL;
		Temp_max : REAL;
		Acu : REAL;
	END_STRUCT;
	MC_Data_typ : 	STRUCT 
		DriveStatus : MC_DRIVESTATUS_TYP;
		ACMotorParSeq : ACP10DATBL_typ;
		ACMotorParAdrRec : ARRAY[0..40]OF ACP10PRADR_typ;
		ACMotorPar : MC_Data_ACMotorPar_typ;
		UDCNominal : REAL;
	END_STRUCT;
	Axis_Cmd_typ : 	STRUCT 
		On : BOOL;
		Home : BOOL;
		EnableSWEndLimits : BOOL;
		MoveVelocity : BOOL;
		MoveAbsolute : BOOL;
		MoveAdditive : BOOL;
		MoveGear : BOOL;
		MoveCam : BOOL;
		MoveCamDwell : BOOL;
		CamDwellLeadIn : BOOL;
		CamDwellLeadOut : BOOL;
		EnableOffset : BOOL;
		EnablePhase : BOOL;
		AutoTune : BOOL;
		ErrorAcknowledge : BOOL;
		DownloadCamProfileData : BOOL;
		DownloadCamProfileObj : BOOL;
		GetCamSlavePosition : BOOL;
		UpdateEncoderScaling : BOOL;
		AutoTuneMode : UINT;
		Distance : REAL;
		Position : REAL;
		Velocity : REAL;
		Acceleration : REAL;
		Deceleration : REAL;
		Direction : USINT;
		HomingMode : USINT;
		MasterIndex : UINT; (*Used by all camming and gearing functions*)
		GearRatioMaster : UINT;
		GearRatioSlave : INT;
		ScalingEncoderCounts : UDINT;
		ScalingMotorRevs : UDINT;
		CamTableID : USINT;
		CamStartMode : USINT;
		CamRatioMaster : REAL;
		CamRatioSlave : REAL;
		CamMasterOffset : REAL;
		CamDwellMasterStartPosition : REAL;
		CamDwellLeadInMasterDistance : REAL;
		CamDwellLeadInSlaveDistance : REAL;
		CamDwellLeadInMasterOffset : REAL;
		CamDwellLeadOutMasterDistance : REAL;
		CamDwellLeadOutSlaveDistance : REAL;
		CamDwellLeadOutMasterOffset : REAL;
		OffsetShift : REAL;
		PhaseShift : REAL;
		AutomatInitParameters : BOOL;
		AutomatUpdateParameters : BOOL;
		AutomatEnable : BOOL;
		AutomatStart : BOOL;
		AutomatStop : BOOL;
		AutomatRestart : BOOL;
		AutomatEnd : BOOL;
		AutomatSignal1 : BOOL;
		AutomatSignal2 : BOOL;
		Automat : MC_AUTDATA_TYP;
		CamProfileName : STRING[12];
		CamProfileData : MC_CAMPROFILE_TYP;
		CamProfileIndex : USINT;
		MasterPosition : REAL;
	END_STRUCT;
	Axis_Data_typ : 	STRUCT 
		Init : BOOL;
		On : BOOL;
		Idle : BOOL;
		Homing : BOOL;
		Homed : BOOL;
		HomingError : BOOL;
		SWEndLimitsEnabled : BOOL;
		MovingVelocity : BOOL;
		MovingAdditive : BOOL;
		MovingAbsolute : BOOL;
		GearingActive : BOOL;
		CammingActive : BOOL;
		CamDwellActive : BOOL;
		AutoTuning : BOOL;
		OffsetShiftAttained : BOOL;
		PhaseShiftAttained : BOOL;
		PhaseActive : BOOL;
		OffsetActive : BOOL;
		Error : BOOL;
		ErrorID : UDINT;
		ErrorText : ARRAY[0..3]OF STRING[79];
		Position : REAL;
		Velocity : REAL;
		AutomatActive : BOOL;
		AutomatRunning : BOOL;
		AutomatStandby : BOOL;
		AutomatActualState : USINT;
		AutomatLastState : USINT;
		Iraw : REAL;
		Ic : REAL;
		Ip : REAL;
		FEraw : REAL;
		FE : REAL;
		MotorSensorTemp : REAL;
		AutoTuneStatus : UINT;
		dig_in_enable : USINT;
		dig_in_neg_hw_end : USINT;
		dig_in_pos_hw_end : USINT;
		dig_in_reference : USINT;
		dig_in_trigger1 : USINT;
		dig_in_trigger2 : USINT;
		nc_obj_typ : UINT;
		acp_typ : USINT;
		UnitBasis : UnitBasis_enum;
		UnitFactor : UDINT;
		AxisPeriod : UDINT;
		PLCopenAxisPeriod : REAL;
		ActualScalingEncoderCounts : UDINT;
		ActualScalingMotorRevs : UDINT;
		LastCamProfileName : STRING[12];
		SlavePosition : REAL;
		MaxWindingTemp : REAL;
		MotorPeakCurrent : REAL;
		MotorRatedCurrent : REAL;
	END_STRUCT;
	Axis_typ : 	STRUCT 
		cmd : Axis_Cmd_typ;
		data : Axis_Data_typ;
	END_STRUCT;
	MM_typ : 	STRUCT 
		cmd : MM_cmd_typ;
		data : MM_data_typ;
	END_STRUCT;
	MM_cmd_typ : 	STRUCT 
		JogNegSlow : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		JogPosSlow : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		JogNegFast : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		JogPosFast : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		JogNegInc : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		JogPosInc : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		Home : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		AutoTune : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		ErrorAcknowledge : BOOL;
	END_STRUCT;
	MM_data_typ : 	STRUCT 
		Mode : Modes_enum;
		Error : BOOL;
		ErrorID : UDINT;
		HomedToSwitch : ARRAY[0..MAX_AXIS_INDEX]OF BOOL;
		AutoTuneStatus : UINT;
		Homed : BOOL;
	END_STRUCT;
END_TYPE
